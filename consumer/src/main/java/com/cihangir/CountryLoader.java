package com.cihangir;

import com.cihangir.schemas.country.Country;
import com.cihangir.schemas.country.GetCountryRequest;
import com.cihangir.schemas.country.GetCountryResponse;
import com.cihangir.schemas.country.ObjectFactory;
import org.springframework.stereotype.Component;
import org.springframework.ws.client.core.WebServiceTemplate;

@Component
public class CountryLoader {

    private final WebServiceTemplate webServiceTemplate;

    public CountryLoader(WebServiceTemplate webServiceTemplate) {
        this.webServiceTemplate = webServiceTemplate;
    }

    public Country load() {
        ObjectFactory factory = new ObjectFactory();
        GetCountryRequest request = factory.createGetCountryRequest();
        request.setName("Poland");

        GetCountryResponse response = (GetCountryResponse) this.webServiceTemplate.marshalSendAndReceive(request);
        System.out.println("response = " + response.getCountry().getCurrency());
        return response.getCountry();
    }
}
