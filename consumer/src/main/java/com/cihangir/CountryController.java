package com.cihangir;

import com.cihangir.schemas.country.Country;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CountryController {

    private final CountryLoader countryLoader;

    public CountryController(CountryLoader countryLoader) {
        this.countryLoader = countryLoader;
    }

    @GetMapping("/country")
    public Country getCountryName() {
        Country country = countryLoader.load();
        return country;
    }
}
