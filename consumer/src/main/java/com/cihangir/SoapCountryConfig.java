package com.cihangir;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.ws.client.core.WebServiceTemplate;

@Configuration
public class SoapCountryConfig {

    @Value("http://localhost:8080/ws/countries")
    private String defaultUri;

    @Bean
    public Jaxb2Marshaller marshaller() {
        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        //Set a JAXB context path.
        //<generatePackage>com.cihangir.schemas.country</generatePackage>
        // this is the package name specified in the <generatePackage> specified in pom.xml
        marshaller.setContextPath("com.cihangir.schemas.country");
        return marshaller;
    }

    @Bean
    public WebServiceTemplate webServiceTemplate() {
        WebServiceTemplate webServiceTemplate = new WebServiceTemplate();
        webServiceTemplate.setMarshaller(marshaller());
        webServiceTemplate.setUnmarshaller(marshaller());
        webServiceTemplate.setDefaultUri(defaultUri);
        return webServiceTemplate;
    }
    /***
     * The central class for client-side Web services.
     * It provides a message-driven approach to sending and receiving.
     *
     *
     *
     *
     */


}
