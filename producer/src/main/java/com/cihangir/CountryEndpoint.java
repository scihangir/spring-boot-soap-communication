package com.cihangir;

import com.cihangir.xml.country.GetCountryRequest;
import com.cihangir.xml.country.GetCountryResponse;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

/***Indicates that an annotated class is an "Endpoint" (e.g. a web service endpoint)
 *  registers the class with Spring WS as a potential candidate for processing incoming SOAP messages.*/
@Endpoint
public class CountryEndpoint {

    private static final String NAMESPACE_URI = "http://cihangir.com/xml/country";

    private final CountryRepository countryRepository;

    public CountryEndpoint(CountryRepository countryRepository) {
        this.countryRepository = countryRepository;
    }


    //@PayloadRoot is then used by Spring WS to pick the handler method based on the message’s namespace and localPart.
    //@RequestPayload indicates that the incoming message will be mapped to the method’s request parameter.
    //The @ResponsePayload annotation makes Spring WS map the returned value to the response payload

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getCountryRequest")
    @ResponsePayload
    public GetCountryResponse getCountry(@RequestPayload GetCountryRequest request) {
        GetCountryResponse response = new GetCountryResponse();
        response.setCountry(countryRepository.findCountry(request.getName()));

        return response;
    }


}
